package main;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL43.*;
import org.lwjgl.glfw.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.time.ZonedDateTime;
import java.util.Random;
import lwjglutils.OGLTextRenderer;
import lwjglutils.OGLUtils;
import lwjglutils.ShaderUtils;

public class Renderer extends AbstractRenderer {

	int computeShaderProgram;
	int locGroupSize, locNumberOfElements, locFirstGroupOffset;
	int[] locBuffer = new int[2];
	FloatBuffer data, dataOut;

	boolean isSortingRunning = false;

	int numbersCount = 20;
	int dataSize, dataSizeInBytes, groupSize = 1, round = 0;
	long sortStartTime;
	float[] dataForSingleThreadSort;

	// If the stepping mode is on (steppingMode=true), each step has to be confirmed
	// by pressing a key. When the key is pressed, the nextStep changes to true
	// and display() step will be performed.
	volatile boolean nextStep = false;
	boolean steppingMode = false;
	boolean printOutput = true;
	private final int textSize = 15;
	private final int upperBoundOfPrintingNumbers = 30;

	@Override
	public void init() {
		OGLUtils.printOGLparameters();

		computeShaderProgram = ShaderUtils.loadProgram(null, null, null, null, null,
				"/main/computeBuffer");

		locGroupSize = glGetUniformLocation(computeShaderProgram, "groupSize");
		locNumberOfElements = glGetUniformLocation(computeShaderProgram, "numberOfElements");
		locFirstGroupOffset = glGetUniformLocation(computeShaderProgram, "firstGroupOffset");

		// declare and generate a buffer object name
		glGenBuffers(locBuffer);

		initForNewRound();

		//assign the index of shader storage block to the binding point (see shader)
		glShaderStorageBlockBinding(computeShaderProgram, 0, 0); //input buffer
		glShaderStorageBlockBinding(computeShaderProgram, 1, 1); //output buffer

		textRenderer = new OGLTextRenderer(width, height);
	}

	private void initForNewRound() {
		if (numbersCount < 2) {
			System.out.println("There have to be at least 2 numbers to sort.");
			return;
		}

		System.out.println("\nSorting on GPU started.");
		isSortingRunning = true;

		// https://stackoverflow.com/questions/29531237/memory-allocation-with-std430-qualifier
		// "each structure has to start at a position multiple of 16"
		// size = count of numbers to sort * (float=4B + padding=3*4B)
		dataSize = numbersCount * (1 + 3);
		dataSizeInBytes = dataSize * 4;

		data = ByteBuffer.allocateDirect(dataSizeInBytes)
				.order(ByteOrder.nativeOrder())
				.asFloatBuffer();
		dataOut = ByteBuffer.allocateDirect(dataSizeInBytes)
				.order(ByteOrder.nativeOrder())
				.asFloatBuffer();

		initBuffer();
		if (printOutput) {
			printBuffer(data);
		}
		groupSize = 1;
		round = 0;

		// bind the buffer and define its initial storage capacity
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, locBuffer[0]);
		glBufferData(GL_SHADER_STORAGE_BUFFER, data, GL_DYNAMIC_DRAW);

		// bind the buffer and define its initial storage capacity
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, locBuffer[1]);
		glBufferData(GL_SHADER_STORAGE_BUFFER, dataOut, GL_DYNAMIC_DRAW);

		// unbind the buffer
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

		sortStartTime = ZonedDateTime.now().toInstant().toEpochMilli();
	}

	private void initBuffer() {
		dataForSingleThreadSort = new float[dataSize];

		data.rewind();
		Random r = new Random();
		for (int i = 0; i < dataSize; i += 4) {
			float newFloat = r.nextFloat();
			data.put(i, newFloat);
			dataForSingleThreadSort[i/4] = newFloat;
		}
	}

	@Override
	public void display() {
		glViewport(0, 0, width, height);

		if (isSortingRunning && (!steppingMode || nextStep)) {
			nextStep = false;
			glUseProgram(computeShaderProgram);

			// set input and output buffer
			if (round % 2 == 0) {
				glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, locBuffer[0]);
				glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, locBuffer[1]);
			} else {
				glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, locBuffer[1]);
				glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, locBuffer[0]);
			}

			final int totalWorkGroups = (int) Math.ceil(numbersCount / (double) groupSize / 2.0);

			int groupSizeAfterMerge = groupSize * 2;
			if (totalWorkGroups > GL_MAX_COMPUTE_WORK_GROUP_COUNT) {
				for (int i = 0; i < totalWorkGroups; i += GL_MAX_COMPUTE_WORK_GROUP_COUNT) {
					int offset = i * groupSizeAfterMerge;
					int currentEvaluationWorkGroups = Math.min(totalWorkGroups - i, GL_MAX_COMPUTE_WORK_GROUP_COUNT);
					compute(offset, currentEvaluationWorkGroups);
				}
			} else {
				compute(0, totalWorkGroups);
			}

			groupSize = groupSizeAfterMerge;
			if (printOutput) {
				glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, dataOut);
				printBuffer(dataOut);
			}

			if (groupSize > numbersCount) {
				printTime();
				glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, dataOut);
				System.out.println("Checking output started.");
				checkOutput();
				System.out.println("Sorting on a single thread started.");
				MergeSort.sort(dataForSingleThreadSort);
				isSortingRunning = false;
			} else {
				round++;
			}
		}

		glUseProgram(0);

		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		updateTextRenderer();
	}

	private void compute(int offset, int workGroups) {
		glUniform1i(locGroupSize, groupSize);
		glUniform1i(locNumberOfElements, numbersCount);
		glUniform1i(locFirstGroupOffset, offset);
		glDispatchCompute(workGroups, 1, 1);
		// make sure writing to buffer has finished before read
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}

	private void printTime() {
		long sortEndTime = ZonedDateTime.now().toInstant().toEpochMilli();
		long sortTime = sortEndTime - sortStartTime;
		System.out.println("Sort time on GPU in milliseconds: " + sortTime);
	}

	private void printBuffer(FloatBuffer buffer) {
		buffer.rewind();
		for (int i = 0; i < Math.min(upperBoundOfPrintingNumbers, numbersCount); i++) {
			System.out.print(" " + (i % groupSize == 0 ? "|" : " ") + " ");
			System.out.print(String.format("%4.2f", buffer.get(i * 4)));
		}
		System.out.println();
	}

	private void checkOutput() {
		dataOut.rewind();
		boolean error = false;
		for (int i = 1; i < numbersCount; i++) {
			float number1 = dataOut.get((i-1) * 4);
			float number2 = dataOut.get(i * 4);

			if (number1 > number2) {
				error = true;
				// System.out.println("Error: " + number1 + " is higher than " + number2 + ".");
			}
		}
		System.out.println("Output is " + (error ? "wrong" : "OK") + ".");
	}

	private void updateTextRenderer() {
		int yOffset = 30;
		textRenderer.clear();

		yOffset = addStringToTextRenderer(textRenderer, yOffset, "Merge sort program. See console for GPU multi thread vs CPU single thread sort times difference." + " " +
				"Try to switch off printing output and setup numbers count to few millions in order to see when GPU sort makes sense." + " " +
				"Remember that you must run next round with [R] key.");
		yOffset = addStringToTextRenderer(textRenderer, yOffset, "");
		yOffset = addStringToTextRenderer(textRenderer, yOffset, "[p]rint output: " + (printOutput ? "true" + " (up to " + upperBoundOfPrintingNumbers + " numbers to the console)" : "false") );
		yOffset = addStringToTextRenderer(textRenderer, yOffset, "[m]ode: " + (steppingMode ? "stepping ([n]ext step) - only makes sense with printing on" : "continuous"));
		yOffset = addStringToTextRenderer(textRenderer, yOffset, "change numbers count: ");
		yOffset = addStringToTextRenderer(textRenderer, yOffset, "  current count: " + String.format("%,d", numbersCount));
		yOffset = addStringToTextRenderer(textRenderer, yOffset, "  [X] = 20");
		yOffset = addStringToTextRenderer(textRenderer, yOffset, "  [A]: +10; [S]: +100; [D]: +1,000; [F]: +10,000; [G]: +100,000; [H]: +1,000,000");
		yOffset = addStringToTextRenderer(textRenderer, yOffset, "new [r]ound");

		if (printOutput && dataOut.capacity()/4 == numbersCount) {
			if (round != 0) {
				glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, dataOut);
				printNumbersWithTextRenderer(dataOut, yOffset);
			} else {
				printNumbersWithTextRenderer(data, yOffset);
			}
		}

		textRenderer.draw();
	}

	void printNumbersWithTextRenderer(FloatBuffer buffer, int yOffset) {
		yOffset = addStringToTextRenderer(textRenderer, yOffset, "");
		yOffset = addStringToTextRenderer(textRenderer, yOffset, "Sorting:");

		int charactersPerLine = 350; // 50 characters per line
		int maxNumberOfLines = 30;
		buffer.rewind();
		String currentLine = "";
		int numberOfLines = 0;
		for (int i = 0; i < numbersCount; i++) {
			String currentCharacters = "";
			currentCharacters += " " + (i % groupSize == 0 ? "|" : " ") + " ";
			currentCharacters += String.format("%4.2f", buffer.get(i * 4));

			boolean fitsToTheCurrentLine = currentLine.length() + currentCharacters.length() <= charactersPerLine;
			if (fitsToTheCurrentLine) {
				currentLine += currentCharacters;
			} else {
				numberOfLines++;
				if (numberOfLines <= maxNumberOfLines) {
					yOffset = addStringToTextRenderer(textRenderer, yOffset, currentLine);
					currentLine = "";
				} else {
					yOffset = addStringToTextRenderer(textRenderer, yOffset, currentLine);
					currentLine = "...";
					break;
				}
			}
		}
		addStringToTextRenderer(textRenderer, yOffset, currentLine);
	}

	// Helper function for updateSettingsAndSetupTextRenderer. Returns new yOffset.
	private int addStringToTextRenderer(OGLTextRenderer textRenderer, int yOffset, String text) {
		int yStepOffset = textSize + 3;
		int xOffset = 15;

		textRenderer.addStr2D(xOffset, yOffset, text);
		return yOffset + yStepOffset;
	}

	private GLFWKeyCallback keyCallback = new GLFWKeyCallback() {
		@Override
		public void invoke(long window, int key, int scancode, int action, int mods) {
			if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
				glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop

			if (action == GLFW_PRESS || action == GLFW_REPEAT) {
				switch (key) {
					case GLFW_KEY_N:
						nextStep = true;
						break;
					case GLFW_KEY_M:
						steppingMode = !steppingMode;
						break;
					case GLFW_KEY_P:
						printOutput = !printOutput;
						break;
					case GLFW_KEY_R:
						initForNewRound();
						break;
					case GLFW_KEY_A:
						changeNumbersCount(10);
						break;
					case GLFW_KEY_S:
						changeNumbersCount(100);
						break;
					case GLFW_KEY_D:
						changeNumbersCount(1_000);
						break;
					case GLFW_KEY_F:
						changeNumbersCount(10_000);
						break;
					case GLFW_KEY_G:
						changeNumbersCount(100_000);
						break;
					case GLFW_KEY_H:
						changeNumbersCount(1_000_000);
						break;
					case GLFW_KEY_X:
						numbersCount = 20;
						break;
				}
			}
		}
	};

	private void changeNumbersCount(int difference) {
		numbersCount += difference;
	}

	private GLFWWindowSizeCallback wsCallback = new GLFWWindowSizeCallback() {
		@Override
		public void invoke(long window, int w, int h) {
			if (w > 0 && h > 0 &&
					(w != width || h != height)) {
				width = w;
				height = h;
				if (textRenderer != null)
					textRenderer.resize(width, height);
			}
		}
	};

	@Override
	public GLFWKeyCallback getKeyCallback() {
		return keyCallback;
	}

	@Override
	public GLFWWindowSizeCallback getWsCallback() {
		return wsCallback;
	}

	@Override
	public GLFWMouseButtonCallback getMouseCallback() {
		return null;
	}

	@Override
	public GLFWCursorPosCallback getCursorCallback() {
		return null;
	}

	@Override
	public GLFWScrollCallback getScrollCallback() {
		return null;
	}

}