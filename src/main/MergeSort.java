package main;

import java.time.ZonedDateTime;

class MergeSort {

	public static void sort(float[] array) {
		long sortStartTime = ZonedDateTime.now().toInstant().toEpochMilli();

		mergeSort(array, array.length);

		long sortEndTime = ZonedDateTime.now().toInstant().toEpochMilli();
		long sortTime = sortEndTime - sortStartTime;
		System.out.println("Sort time on a single thread in milliseconds: " + sortTime);
	}

	private static void mergeSort(float[] outputArray, int numberOfElements) {
		if (numberOfElements < 2) {
			return;
		}
		int middle = numberOfElements / 2;
		float[] leftArray = new float[middle];
		float[] rightArray = new float[numberOfElements - middle];

		System.arraycopy(outputArray, 0, leftArray, 0, middle);
		System.arraycopy(outputArray, middle, rightArray, 0, numberOfElements - middle);

		mergeSort(leftArray, middle);
		mergeSort(rightArray, numberOfElements - middle);

		merge(outputArray, leftArray, rightArray, middle, numberOfElements - middle);
	}

	private static void merge(float[] outputArray, float[] leftGroup, float[] rightGroup, int left, int right) {
		int i = 0, j = 0, k = 0;
		while (i < left && j < right) {
			if (leftGroup[i] <= rightGroup[j]) {
				outputArray[k++] = leftGroup[i++];
			} else {
				outputArray[k++] = rightGroup[j++];
			}
		}
		while (i < left) {
			outputArray[k++] = leftGroup[i++];
		}
		while (j < right) {
			outputArray[k++] = rightGroup[j++];
		}
	}

}