# KPGR3 - Task 2

\#merge, \#sort, \#GPGPU, \#compute, \#shader, \#java, \#glsl, \#lwjgl

## Task
GPGPU (General-purpose computing on graphics processing units) implemented with compute shader. Specifically implementation of the 
**merge sort** algorithm. In addition, a comparison with a CPU single thread sorting is added.

## Installing 
There is an attached tutorial JakNaPGRF.pdf. This file will guide you to setup the environment. 

The code is tested on Windows.

##### Main steps (in IntelliJ idea):
- In project structure right-click on the "res" and "shaders" folders and select "Mark Directory as" - "Resources Root"
-  Customize and download LWJGL utils for your platform here: [https://www.lwjgl.org/customize](https://www.lwjgl.org/customize). You will need these modules: LWJGL-core, GLFW, Native File Dialog, Nuklear, OpenGL, stb and Tiny File Dialogs. This source code is guaranteed to run with version 3.2.3.
- Open "File" - "Project Structure" - "Global Libraries". Click on plus (New Global Library) and select Java
- Choose .jar files from unzipped downloaded LWJGL utils.
- Check modules in Project Structure. There should be the added global library in the dependencies. If not, click on the plus in dependencies and select "Library" - "Java" and choose the added library.
- In project structure find "src" - "main" - "App", right click on it and select Run App.main

## Control
- **P**: Print output
- **M**: Mode: stepping mode / continuous mode
- Changing count of numbers to sort:
  - **X** = 20
  - **A**: +10; **S**: +100; **D**: +1,000; **F**: +10,000; **G**: +100,000; **H**: +1,000,000
- **R**: New round